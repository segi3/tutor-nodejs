const mysql = require('mysql')
const config = require('../config.json')

const pool = mysql.createPool({
    host: config.SQL_HOST,
    user: config.SQL_USER,
    password: config.SQL_PASSWORD,
    database: config.SQL_DATABASE,
    connectionLimit: '10'
})

module.exports = {
    pool
}