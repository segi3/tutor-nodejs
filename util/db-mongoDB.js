const mongoose = require('mongoose')
const config = require('../config.json')

// ambil string connection dari dashboard mongoDB atlas
const dbURI = config.MONGODB_URI

mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log('succesfully connected to mongoDB')
    })
    .catch((err) => {
        console.log(err)
    })

