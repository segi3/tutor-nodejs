const express = require('express')
const sqlProductController = require('../controller/sqlProductController') //import file controller


const router = express.Router() // tambah endpoints di router express

// daaftar route product pake sql
router.post('/get-products', sqlProductController.getProducts)
router.post('/create-product', sqlProductController.createProduct)
router.post('/delete-product', sqlProductController.deleteProduct)
router.post('/update-product', sqlProductController.updateProduct)

module.exports = router // export router