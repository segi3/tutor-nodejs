const express = require('express')
const nosqlProductController = require('../controller/nosqlProductController') //import file controller


const router = express.Router() // tambah endpoints di router express

// daftar route product pake sql
router.post('/create-product', nosqlProductController.createProduct)
router.post('/get-product', nosqlProductController.getProductById)
router.post('/update-product', nosqlProductController.updateProduct)

module.exports = router // export router