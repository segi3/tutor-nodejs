const mysql = require('../util/db-mysql')
const pool = mysql.pool

const { v1: uuidv1 } = require('uuid')

//controller bentuk nya fungsi yang di export untuk dipake di route

const getProducts = (req, res) => {

    pool.getConnection((err, conn) => {

        if (err) {
            console.log(err)
            res.status(500).send(err)
        }

        conn.query('select * from products', (err, rows) => {

            // error handling
            if (err) {
                console.log(err)
                conn.release() // hapus koneksi
                res.status(500).send(err)
            }
            
            res.status(200).send(rows)
        })
    })

}

const createProduct = (req, res) => {

    console.log(req.body)

    const product_data = {
        id: uuidv1(),
        nama: req.body.nama,
        harga: req.body.harga
    }

    pool.getConnection ((err, conn) => {
        conn.query('insert into products set ?', product_data, (err, row) => {

            // error handling
            if (err) {
                console.log(err)
                conn.release() // hapus koneksi
                res.status(500).send(err)
            }
    
            res.status(200).send({
                message: 'berhasil insert',
                produk: product_data
            })
        })
    })
}

const deleteProduct = (req, res) => {

    const product_data = {
        id: req.body.id
    }

    pool.getConnection((err, conn) => {

        const sql = 'delete from products where id = ?'
        const sql_arr = [product_data.id]

        conn.query(sql, sql_arr, (err, row) => {

            // error handling
            if (err) {
                console.log(err)
                conn.release() // hapus koneksi
                res.status(500).send(err)
            }

            res.status(200).send({
                message: 'berhasil dihapus'
            })
        })
    })
}

const updateProduct = (req, res) => {

    const product_data = {
        id: req.body.id,
        nama: req.body.nama,
        harga: req.body.harga
    }

    pool.getConnection((err, conn) => {

        const sql = 'update products set nama=?, harga=? where id=?'
        const sql_arr = [product_data.nama, product_data.harga, product_data.id]

        conn.query(sql, sql_arr, (err, row) => {

            // error handling
            if (err) {
                console.log(err)
                conn.release() // hapus koneksi
                res.status(500).send(err)
            }

            res.status(200).send({
                message: 'berhasil di update',
                produk: product_data
            })
        })
        
    })
}

module.exports = {
    getProducts,
    createProduct,
    deleteProduct,
    updateProduct,
}