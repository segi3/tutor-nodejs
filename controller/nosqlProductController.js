const Product = require('../models/product')

const createProduct = async (req, res) => {

    const product_data = { // contoh data yang dibuat
        nama: 'mouse',
        harga: 10000
    }

    try {

        const result = await Product(product_data).save()

        res.status(200).send(result)

    } catch (err) {
        console.log(err)

        res.status(500).send({
            message: 'tidak berhasil create'
        })

    }

}

const getProductById = async (req, res) => {

    const product_data = { // contoh data yang dicari
        id: '60e92619e85a85667592bb1e',
        nama: 'mouse'
    }

    try {

        const result = await Product.find({
            _id: product_data.id
        }, (err, res) => {

            if (err) console.log(err)

            console.log(res)
        })

    } catch (err) {

        console.log(err)
        
        res.status(500).send({
            message: 'tidak berhasil create'
        })
    }
}

const updateProduct = async (req, res) => {

    const product_data = { // contoh data
        id: '60e92619e85a85667592bb1e',
        nama: 'mouse'
    }

    try {

        const result = await Product.findOneAndUpdate({
            nama: 'komputer'
        }, {
            harga: 50000
        }, {
            new: true,
            upsert: true
        })

        console.log(result)

    } catch (err) {
        console.log(err)
        
        res.status(500).send({
            message: 'tidak berhasil create'
        })
    }

}

module.exports = {
    createProduct,
    getProductById,
    updateProduct,
}