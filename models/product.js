const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema ({
    nama: {
        type: String,
        required: true
    },
    harga: {
        type: Number,
        required: true
    }
})

const Product = mongoose.model('product', productSchema)

module.exports = Product