const express = require('express')
const app = express()

const PORT = 3000

// nge rubah body form jadi json object
app.use(require('body-parser').urlencoded({
    extended: false
}))

// inisiasi koneksi ke mongoDB (singleton) cuma perlu dipanggil sekali
// uncomment code dibawah untuk make mongoDB
// const mongoDB = require('./util/db-mongoDB')

// menggunakan route
const sqlProductRoutes = require('./routes/sqlProductRoute')
const nosqlProductRoutes = require('./routes/nosqlProductController')

app.use('/api/sql', sqlProductRoutes) // prefix untuk yang pake sql
app.use('/api/nosql', nosqlProductRoutes) // prefix untuk yang pake nosql

app.listen(PORT, () => {
    console.log(`Server is now listening at localhost:${PORT}..`)
})