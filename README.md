## File tutor NodeJS pertemuan-3

instruksi pemakaian:

1. clone repository

```git
git clone https://gitlab.com/segi3/tutor-nodejs
```

2. install depedencies project (akan otomatis menginstall semua module di `package.json`)

```
npm install
```

3. Untuk mysql buat tabel dengan nama `product`

```sql
create table `products`
(
    `id` VARCHAR(255) NOT NULL,
    `nama` VARCHAR(255) NOT NULL,
    `HARGA` INT NOT NULL,
    PRIMARY KEY (`id`)
);
```

ubah nama file `example-config.json` menjadi `config.json`, isi dengan kredensial database user sql

```json
{  
    "SQL_HOST" : "localhost",
    "SQL_USER" : "root",
    "SQL_PASSWORD" : "",
    "SQL_DATABASE" : "tutor_node",

    ...
}
```

4. Untuk menggunakan mongoDB, uncomment koneksi mongoDB di `app.js`

```js
const mongoDB = require('./util/db-mongoDB')
```
<br/>

lalu buat akun dan ambil string koneksi dari dashboard mongoDB atlas

Setelah dapat string koneksi, masukkan ke dalam file `config.json` (jika masih `example-config.json` silahkan dirubah namanya)

```json
{  
    ...

    "MONGODB_URI" : ""
}
```

4. jalankan aplikasi

```
node app
```


### Versi

- Sabtu 10 juli, 13.16
```
Update hasil tutor di repo
```

- Senin 12 juli, 21.19
```
Struktur Model dan Controller
implementasi variabel environment
```
